var Papa = require('papaparse');
const fs = require('fs');
var variance = require( 'compute-variance' );

//const file = fs.createReadStream('data/Borsdata_2018-09-25-momentum.csv');
const file = fs.createReadStream('data/Borsdata_2018-10-06.csv');

/*****************************/
/* LOAD DATA                 */
/*****************************/

Papa.parse(file, {
    header: true,
    comments: "Bolagsnamn",
    dynamicTyping: true,
    skipEmptyLines: true,
    complete: function(result) {
        //console.log(JSON.stringify(result.data,null,4))
        compositeMomentum(result.data)
    },
    error: err => console.err(err),
});


/*****************************/
/* CALCULATE MOMENTUM        */
/*****************************/

function compositeMomentum(data){

    //console.log(data)

    // Curate company momentum data

    var cMomentum = []

    data.forEach( el => {
        obj = {
            'name':el['Ticker'],
            'company': el['Bolagsnamn'],
            '3m': el['Utveck.3m'],
            '6m': el['Utveck.6m'],
            '12m': el['Utveck.12m'],
            'rank': [], //3, 6, 12
            'meanRank':0
        }
        cMomentum.push(obj)
    })

    var localData = data.slice(0)

    
    // ---------- 3-month momentum ---------------
    localData.sort(function(a,b){
        return b['Kursutveck.3m']-a['Kursutveck.3m']
    })
    //console.log('---------')

    localData.forEach( (el,index) => {

        //console.log(el.Ticker) 
        
        cMomentum.filter(obj => obj.name == el.Ticker).map(obj => {
            obj.rank.push(index+1);
            obj.meanRank+=(index+1)/3
        })              
        
    })

   // console.log(JSON.stringify(cMomentum,null,2))

   // ---------- 6-month momentum ---------------
    localData.sort(function(a,b){
        return b['Kursutveck.6m']-a['Kursutveck.6m']
    })
    //console.log('---------')

    localData.forEach( (el,index) => {

        //console.log(el.Ticker) 
        
        cMomentum.filter(obj => obj.name == el.Ticker).map(obj => {
            obj.rank.push(index+1);
            obj.meanRank+=(index+1)/3
        })                             
    })

   // console.log(JSON.stringify(cMomentum,null,2))

   // ---------- 12-month momentum ---------------
    localData.sort(function(a,b){
        return b['Kursutveck.12m']-a['Kursutveck.12m']
    })
    //console.log('---------')

    localData.forEach( (el,index) => {

        //console.log(el.Ticker) 
        
        cMomentum.filter(obj => obj.name == el.Ticker).map(obj => {
            obj.rank.push(index+1);
            obj.meanRank+=(index+1)/3
        })                             
    })

    //console.log(JSON.stringify(cMomentum,null,2))


    // Sort on meanRank
    cMomentum.sort(function(a,b){
        return a.meanRank-b.meanRank
    })

    //console.log(JSON.stringify(cMomentum,null,2))
    cMomentum.forEach( (el,index) => console.log(index+1, el.name, el.company, el.rank, 
        '(meanRank=',Math.round(el.meanRank*100,4)/100,
        'stdRank=',Math.round(Math.sqrt(variance(el.rank))*100,4)/100,')')
    )
    

    
}



